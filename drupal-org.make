api = 2
core = 7.x

; Contributed modules.

projects[addressfield][version] = "1.0-beta4"
projects[addressfield][subdir] = "contrib"

projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][subdir] = "contrib"

projects[advanced_help][version] = "1.0"
projects[advanced_help][subdir] = "contrib"

projects[auto_nodetitle][version] = "1.0"
projects[auto_nodetitle][subdir] = "contrib"

projects[ctools][version] = "1.3"
projects[ctools][subdir] = "contrib"
; projects[ctools][patch][] = http://drupal.org/files/ctools-dependent-js-broken-with-jquery-1.7-1494860-30.patch

projects[commerce][version] = "1.7"
projects[commerce][subdir] = "contrib"

projects[commerce_features][version] = "1.0-rc1"
projects[commerce_features][subdir] = "contrib"

projects[commerce_registration][version] = "2.0-beta5"
projects[commerce_registration][subdir] = "contrib"

projects[commerce_stock][version] = "1.2"
projects[commerce_stock][subdir] = "contrib"

projects[content_access][version] = "1.2-beta2"
projects[content_access][subdir] = "contrib"

projects[conditional_fields][version] = "3.x-dev"
projects[conditional_fields][subdir] = "contrib"

projects[context][version] = "3.0-beta6"
projects[context][subdir] = "contrib"

projects[date][version] = "2.6"
projects[date][subdir] = "contrib"

projects[devel][version] = "1.x-dev"
projects[devel][subdir] = "contrib"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

projects[email_registration][version] = "1.1"
projects[email_registration][subdir] = "contrib"

projects[entity][version] = "1.1"
projects[entity][subdir] = "contrib"

projects[entityreference][version] = "1.0"
projects[entityreference][subdir] = "contrib"

projects[entityreference_prepopulate][version] = "1.3"
projects[entityreference_prepopulate][subdir] = "contrib"

projects[features][version] = "1.0"
projects[features][subdir] = "contrib"

projects[field_group][version] = "1.1"
projects[field_group][subdir] = "contrib"

projects[fivestar][version] = "2.0-alpha2"
projects[fivestar][subdir] = "contrib"
projects[fivestar][patch][] = http://drupal.org/files/entityreference-support-1488914-15.patch

projects[field_permissions][version] = "1.0-beta2"
projects[field_permissions][subdir] = "contrib"

projects[flag][version] = "2.1"
projects[flag][subdir] = "contrib"

projects[inline_entity_form][version] = "1.2"
projects[inline_entity_form][subdir] = "contrib"

projects[inline_registration][version] = "1.x-dev"
projects[inline_registration][subdir] = "contrib"
projects[inline_registration][patch] = http://drupal.org/files/272672-email-registration-23.patch

projects[jquery_update][version] = "2.3"
projects[jquery_update][subdir] = "contrib"

projects[link][version] = "1.1"
projects[link][subdir] = "contrib"

projects[panels][version] = "3.3"
projects[panels][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[profile_field_search][version] = "1.x-dev"
projects[profile_field_search][subdir] = "contrib"

projects[quicktabs][version] = "3.4"
projects[quicktabs][subdir] = "contrib"

projects[realname][version] = "1.1"
projects[realname][subdir] = "contrib"

projects[registration][version] = "1.1"
projects[registration][subdir] = "contrib"

projects[rules][version] = "2.3"
projects[rules][subdir] = "contrib"

projects[strongarm][version] = "2.x-dev"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"

projects[views][version] = "3.7"
projects[views][subdir] = "contrib"

projects[views_field_view][version] = "1.0"
projects[views_field_view][subdir] = "contrib"

projects[views_send][version] = "1.0-rc3"
projects[views_send][subdir] = "contrib"

projects[views_data_export][version] = "3.0-beta6"
projects[views_data_export][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.1"
projects[views_bulk_operations][subdir] = "contrib"

projects[votingapi][version] = "2.11"
projects[votingapi][subdir] = "contrib"

projects[webform][version] = "3.18"
projects[webform][subdir] = "contrib"

; Git sourced Modules
projects[converge_support][type] = "module"
projects[converge_support][subdir] = "contrib"
projects[converge_support][download][type] = "git"
projects[converge_support][download][url] = "http://git.drupal.org/project/converge_support.git"
projects[converge_support][download][branch] = "7.x-1.x"

; Throw this sandbox review module into converge_support.
projects[cod_session_reviews][type] = "module"
projects[cod_session_reviews][subdir] = "contrib/converge_support"
projects[cod_session_reviews][download][type] = "git"
projects[cod_session_reviews][download][url] = "http://git.drupal.org/sandbox/cafuego/1872914.git"
projects[cod_session_reviews][download][branch] = "7.x-1.x"

projects[admin_icons][type] = "module"
projects[admin_icons][subdir] = "contrib"
projects[admin_icons][download][type] = "git"
projects[admin_icons][download][url] = "http://git.drupal.org/project/admin_icons.git"
projects[admin_icons][download][branch] = "7.x-1.x"

projects[uuid][type] = "module"
projects[uuid][subdir] = "contrib"
projects[uuid][download][type] = "git"
projects[uuid][download][url] = "http://git.drupal.org/project/uuid.git"
projects[uuid][download][branch] = "7.x-1.x"

projects[uuid_features][type] = "module"
projects[uuid_features][subdir] = "contrib"
projects[uuid_features][download][type] = "git"
projects[uuid_features][download][url] = "http://git.drupal.org/project/uuid_features.git"
projects[uuid_features][download][branch] = "7.x-1.x"

projects[chaos_plural_plugins][type] = "module"
projects[chaos_plural_plugins][subdir] = "contrib"
projects[chaos_plural_plugins][download][type] = "git"
projects[chaos_plural_plugins][download][url] = "http://git.drupal.org/sandbox/cafuego/1979270.git"
projects[chaos_plural_plugins][download][branch] = "7.x-1.x"

projects[views_access_callback][type] = "module"
projects[views_access_callback][subdir] = "contrib"
projects[views_access_callback][download][type] = "git"
projects[views_access_callback][download][url] = "http://git.drupal.org/sandbox/cafuego/1979346.git"
projects[views_access_callback][download][branch] = "7.x-1.x"

; Themes.

projects[tao][version] = "3.0-beta4"
projects[tao][subdir] = "contrib"

projects[rubik][version] = "4.0-beta8"
projects[rubik][subdir] = "contrib"

projects[bootstrap][version] = "2.0"
projects[bootstrap][subdir] = "contrib"
projects[bootstrap][patch][] = "http://drupal.org/files/bootstrap-library-2.2.2_0.patch"
