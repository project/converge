<?php

/**
 * Implements hook_install_tasks().
 */
function converge_install_tasks($install_state) {
  $tasks = array();

  // Make sure we have more memory than 196M. if not lets try to increase it.
  // @todo: Stick this in a hook_requirements.
  if (ini_get('memory_limit') != '-1' && ini_get('memory_limit') <= '196M') {
    ini_set('memory_limit', '196M');
  }
}

/**
 * Helper function defines the COD modules. -- will use this later so people can select COD features on install. Not useful right now
 */
function _converge_profile_modules() {
  return array(
    'converge_base',
    'converge_session',
    'converge_events',
    'converge_community',
    'converge_front_page',
    'converge_news',
    'converge_sponsors',
  );
}

/**
 * Implements hook_form_alter().
 */
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  foreach ($form['profile'] as $key => $element) {
    $form['profile'][$key]['#value'] = 'converge';
  }
}
