api = 2
core = 7.x
projects[drupal][version] = 7.20

includes[] = drupal-org-core.make
includes[] = drupal-org.make

projects[converge][version] = 7.x-1.x
projects[converge][type] = profile
projects[converge][download][type] = git
projects[converge][download][url] = http://git.drupal.org/project/converge.git
projects[converge][download][branch] = 7.x-1.x
