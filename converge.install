<?php

/**
 * Implements hook_requirements().
 */
function converge_requirements($phase) {
  $requirements = array();
  $t = get_t();

  $memory = ini_get('memory_limit');

  if ($memory != '-1' && $memory < '192M') {
    $requirements['converge'] = array(
      'title' => $t('Converge'),
      'value' => $t('Memory limit is too low.'),
      'description' => $t('The current PHP memory limit setting of @memory is too low, which can cause problems. Please edit your php.ini file and set <em>memory_limit</em> to a minimum of 192M.', array('@memory' => $memory)),
      'severity' => REQUIREMENT_WARNING,
    );
  }

  return $requirements;
}

/**
 * Implements hook_install().
 */
function converge_install() {
  // Create page type.

  // Insert default pre-defined node types into the database. For a complete
  // list of available node type attributes, refer to the node type API
  // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Basic page'),
      'base' => 'node_content',
      'description' => st("Use <em>basic pages</em> for your static content, such as an 'About us' page."),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
  );

  foreach ($types as $type) {
    $type = node_type_set_defaults($type);
    node_type_save($type);
    node_add_body_field($type);
  }

  // Default page to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_HIDDEN);

  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);

  // Themes.
  $default_theme = 'bootstrap';
  $admin_theme = 'rubik';

  // Enable the default theme.
  db_update('system')
    ->fields(array('status' => 1))
    ->condition('type', 'theme')
    ->condition('name', $default_theme)
    ->execute();
  variable_set('theme_default', $default_theme);

  // Disable the bartik theme, we won't be using it.
  db_update('system')
    ->fields(array('status' => 0))
    ->condition('type', 'theme')
    ->condition('name', 'bartik')
    ->execute();

  // Enable the admin theme.
  db_update('system')
    ->fields(array('status' => 1))
    ->condition('type', 'theme')
    ->condition('name', $admin_theme)
    ->execute();
  variable_set('admin_theme', $admin_theme);

  // Disable per-user timezone settings. That is a sure-fire way to mess up
  // scheduling on any event site where contributors live in more than one tz.
  variable_set('configurable_timezones', 0);

  // Use admin theme when editing content.
  variable_set('node_admin_theme', '1');

  // Enable some standard blocks.
  $blocks = array(
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $admin_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'cache' => -1,
    ),
  );
  $query = db_insert('block')->fields(array('module', 'delta', 'theme', 'status', 'weight', 'region', 'pages', 'cache'));
  foreach ($blocks as $block) {
    $query->values($block);
  }
  $query->execute();

  // Revert features to be sure everything is setup correctly.
  // Currently disabled because we don't want devs enabling them on startup
  /*
  $revert = array(
    'converge_base' => array('variable'),
    'converge_community' => array('variable'),
    'converge_events' => array('variable'),
    'converge_news' => array('variable'),
    'converge_session' => array('variable'),
    'converge_sponsors' => array('variable'),
  );
  features_revert($revert);
  */
  node_access_rebuild(FALSE);

}
