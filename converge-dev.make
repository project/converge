; Converge snapshot drush makefile.
;
; Based on the COD dev snapshot makefile.
;
; The purpose of this makefile is to make it easier for people to install
; the dev version of COD and its dependencies, including patches, before
; a new full release of the distribution is rolled.
api = 2
core = 7.20

projects[drupal] = 7.20

projects[converge][type] = "module"
projects[converge][download][type] = "git"
projects[converge][download][url] = "http://git.drupal.org/project/converge.git"
projects[converge][download][branch] = "7.x-1.x"
